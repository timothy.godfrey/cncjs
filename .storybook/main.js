module.exports = {
  stories: [
    '../stories/**/*.stories.js',
    '../src/app/components/**/*.stories.js',
    '../src/app/widgets/**/*.stories.js'
  ],
  addons: ['@storybook/addon-actions', '@storybook/addon-links'],
  webpackFinal: async config => {
    // do mutation to the config

    return config;
  },
};
