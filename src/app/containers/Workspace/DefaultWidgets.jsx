import classNames from 'classnames';
import ensureArray from 'ensure-array';
import React, { PureComponent } from 'react';
import store from 'app/store';
import Widget from './Widget';
import styles from './widgets.styl';

class DefaultWidgets extends PureComponent {
    render() {
        const { className } = this.props;
        const defaultWidgets = ensureArray(store.get('workspace.container.default.widgets'));
        const widgets = defaultWidgets.map(widgetId => (
            <div data-widget-id={widgetId} key={widgetId} style={{ height: '50%' }}>
                <Widget
                    style={{ height: '100%' }}
                    widgetId={widgetId}
                />
            </div>
        ));
        widgets.push(
            <div data-widget-id="webcam" key="webcam" style={{ height: '50%' }}>
                <Widget
                    widgetId="webcam"
                    style={{ height: '100%' }}
                    onFork={() => {
                        console.log('Fork the webcam widget');
                    }}
                    onRemove={() => {
                        console.log('remove the webcam widget');
                    }}
                    sortable={{
                        handleClassName: 'sortable-handle',
                        filterClassName: 'sortable-filter'
                    }}
                />
            </div>
        );

        return (
            <div className={classNames(className, styles.widgets)}>
                {widgets}
            </div>
        );
    }
}

export default DefaultWidgets;
