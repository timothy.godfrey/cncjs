import React from 'react';
import { Button } from '../Buttons';

const containerStyle = {
  display: 'grid',
  gridTemplateColumns: 'repeat(3, 1fr)',
  gridTemplateRows: '1fr 1fr',
  gridGap: '1em'
};

const numberStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  gridColumn: '1 / 3',
  gridRow: '1 / 3',
  borderStyle: 'solid',
  borderWidth: '1px',
  borderRadius: '5px',
  borderColor: 'gray',
  height: '100%',
  width: '100%',
};

const incrementStyle = {
  gridColumn: '3 / 4',
  gridRow: '1 / 2',
  alignSelf: 'start',
};

const decrementStyle = {
  gridColumn: '3 / 4',
  gridRow: '2 / 3',
  alignSelf: 'end',
};

const textStyle = {
  fontSize: '24',
};

function TouchSpinner (props) {
  return (
    <div style={containerStyle}>
      <div style={numberStyle}>
        <div style={textStyle}>{Number.parseFloat(props.value).toFixed(1)}</div>
      </div>
      <div style={incrementStyle}>
        <Button>+</Button>
      </div>
      <div style={decrementStyle}>
        <Button>-</Button>
      </div>
    </div>

  );
}

export default TouchSpinner;
