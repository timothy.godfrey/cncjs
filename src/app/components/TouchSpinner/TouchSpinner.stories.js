import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import TouchSpinner from './TouchSpinner';

export default {
  title: 'TouchSpinner',
  component: TouchSpinner,
};

export const OnePointZero = () => <TouchSpinner value={1.0} />;
export const OnePointOne = () => <TouchSpinner value={1.1} />;
