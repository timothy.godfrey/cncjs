import React from 'react';
import ReactDOM from 'react-dom';
import GcodeGenerator from './GcodeGenerator';

export default () => {
    ReactDOM.render(
        <GcodeGenerator />,
        document.getElementById('viewport')
    );
};
